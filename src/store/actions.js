import axios from 'axios';
export const START_REQUEST = 'START_REQUEST';
export const SUCCESS_REQUEST = 'SUCCESS_REQUEST';
export const ERROR_REQUEST = 'ERROR_REQUEST';

export const startRequest = () => {
    return {type: START_REQUEST}
};

export const successRequest = (response) => {
    return {type: SUCCESS_REQUEST, value: response};
};

export const errorRequest = () => {
    return {type: ERROR_REQUEST};
};

export const fetchList = () => {
    return dispatch => {
        dispatch(startRequest());
        axios.get('https://www.reddit.com/r/pics.json').then(response => {
            dispatch(successRequest(response.data.data.children));
        }, error => {
            dispatch(errorRequest());
        });
    }
};