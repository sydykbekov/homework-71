import {ERROR_REQUEST, START_REQUEST, SUCCESS_REQUEST} from "./actions";

const initialState = {
    response: [],
    loading: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case START_REQUEST:
            return {...state, loading: true};
        case SUCCESS_REQUEST:
            return {...state, response: state.response.concat(action.value), loading: false};
        case ERROR_REQUEST:
            return {...state, loading: false};
        default:
            return state;
    }
};

export default reducer;