import React, {Component} from 'react';
import {connect} from "react-redux";
import {Image, Text, View, StyleSheet, FlatList} from "react-native";
import {fetchList} from "../../store/actions";
import Preloader from '../../assets/Preloader.gif';

class List extends Component {
    componentDidMount() {
        this.props.fetchList();
    }
    render() {
        let div;
        if (this.props.loading) {
            div = <View style={styles.preloader}><Image source={Preloader} /></View>
        } else {
            div = (
                <FlatList
                    data={this.props.response}
                    keyExtractor={(item) => item.data.id}
                    renderItem={({item}) =>
                        <View style={styles.listContainer}>
                            <Image style={styles.image} source={{uri: item.data.thumbnail}} />
                            <Text style={styles.text}>{item.data.title}</Text>
                        </View>
                    }
                />
            )
        }
        return (
            <View style={styles.container}>
                {div}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 50,
        alignItems: 'center'
    },
    listContainer: {
        width: '100%',
        backgroundColor: 'black',
        padding: 10,
        marginTop: 5,
        flexDirection: 'row',
        alignItems: 'center'
    },
    image: {
        width: '30%',
        height: 80,
        borderRadius: 5
    },
    text: {
        width: '70%',
        padding: 10,
        color: 'grey'
    },
    preloader: {
        marginTop: '50%'
    }
});

const mapStateToProps = state => {
    return {
        response: state.response,
        loading: state.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchList: () => dispatch(fetchList())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(List);