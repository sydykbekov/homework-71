import React from 'react';

import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';

import reducer from './src/store/reducer';
import List from "./src/containers/List/List";
import thunk from 'redux-thunk';

const store = createStore(reducer, applyMiddleware(thunk));


const App = props => (
    <Provider store={store}>
        <List />
    </Provider>
);

export default App;